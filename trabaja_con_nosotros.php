<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>Integrador de tecnología de la información | Integrador Technology</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="autor" content="Luis Fernando Jonathan Vargas Osornio - vojohn95@gmail.com">
    <meta name="keywords"
          content="tecnología , desarrollo movil, desarrollo web, aplicaciones, java, php, android, .net, integradores de tecnología,integradores tecnología,grupo empresarial,soluciones TI,Alianzas estrategicas,empresa mexicana,desarrollo de lideres,mejoramiento economico,desarrollo de empresarios, ">
    <meta name="revisit-after" content="14 days">
    <meta name="distribution" content="global">
    <meta name="robots" content="Index, Follow">
    <meta name="google-site-verification" content="DMtsHMbyFRC7HGLZzte2bw3SEOF0miSyzoPiMt2zVJs"/>
    <meta http-equiv="expires" content="3600"/>
    <link rel="canonical" href="https://integrador-technology.mx"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta content="Integrador de tecnología de la información | Integrador Technology<"/>
    <meta content="Integrador de tecnología de la información | Integrador Technology<" property="og:title">
    <meta content="https://integrador-technology.mx" property="og:url">
    <meta content="website" property="og:type">
    <meta name="copyright" content="Integrador Technology"/>
    <meta property="og:locale" content="es_MX">
    <meta property="og:image" content="https://integrador-technology.mx/img/logo.png">
    <meta property="og:description"
          content="Integrador de tecnología, Centros de Computo, Plataformas, Servidores, Sistemas Operativos, Bases de datos, Aplicaciones, Replicación, Almacenamiento, Manejadores de volúmenes, Monitoreo, Alta disponibilidad, Particiones, Virtualización, Clusterización, Cloud, Respaldos, Encriptación, Seguridad, Redes, Comunicaciones, Cableado, Atención a usuarios, programación, Outsourcing y Consultoría, reduciendo costos, garantizando la integridad, seguridad y confiabilidad de la información y asegurando la continuidad de las operaciones de los servicios proporcionados a nuestros clientes .">
    <meta property="og:site_name" content="Integrador Technology">
    <meta name="description"
          content="
Integrador de Tecnología de la información, en Centros de Computo, Plataformas, Servidores, Sistemas Operativos, Bases de datos, Aplicaciones, Replicación, Almacenamiento, Manejadores de volúmenes, Monitoreo, Alta disponibilidad, Particiones, Virtualización, Clusterización, Cloud, Respaldos, Encriptación, Seguridad, Redes, Comunicaciones, Cableado, Atención a usuarios, programación, Outsourcing y Consultoría, reduciendo costos, garantizando la integridad, seguridad y confiabilidad de la información y asegurando la continuidad de las operaciones de los servicios proporcionados a nuestros clientes ."/>
    <meta name="keywords"
          content="tecnología , desarrollo movil, desarrollo web, aplicaciones, java, php, android, .net, integradores de tecnología,integradores tecnología,grupo empresarial,soluciones TI,Alianzas estrategicas,empresa mexicana,desarrollo de lideres,mejoramiento economico,desarrollo de empresarios, it services, system integrators, erp software, outsourcing strategy, strategic sourcing, offshore software development, outsourcing companies, global sourcing, application outsourcing services, enterprise application portfolio management"/>


    <style>
        html, body, header, .intro-4 {
            height: 40%
        }

        @media (max-width: 450px) {
            html, body, header, .intro-4 {
                height: 700px
            }
        }

        @media (min-width: 451px) and (max-width: 740px) {
            html, body, header, .intro-4 {
                height: 500px
            }
        }

        @media (min-width: 800px) and (max-width: 850px) {
            html, body, header, .intro-4 {
                height: 550px
            }
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-148801941-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-148801941-1');
    </script>

</head>

<body>

<!-- Navigation & Intro -->
<header>

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top scrolling-navbar" style="background-color:#737374">
        <div class="container">
            <a class="navbar-brand font-weight-bold title" href=""><img src="img/logo.png" alt="Integrador Technology"
                                                                        style="width: 80%;"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                    aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="list-unstyled navbar-nav mr-auto smooth-scroll">
                    <li class="nav-item">
                        <a class="nav-link white-text" href="index.html">INICIO
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>

</header>
<!-- Navigation & Intro -->

<!-- Main content -->
<main>

    <div class="container">
        <section id="contact" class="text-center my-5">

            <!-- Section heading -->
            <h3 class="text-center title my-5 dark-grey-text font-weight-bold wow fadeIn" data-wow-delay="0.2s">
                <strong>Trabaja con nosotros</strong>
            </h3>

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3764.407082642676!2d-99.20345148469505!3d19.351518386930547!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d2001082ccd883%3A0x73b3f38fbe0bc25!2sBlvd. Adolfo L%C3%B3pez Mateos 2349%2C Atlamaya%2C 01760 Ciudad de M%C3%A9xico%2C CDMX!5e0!3m2!1ses-419!2smx!4v1522813950315"
                    width="100%" height="380" frameborder="0" style="border:0" allowfullscreen></iframe>
            <br><br>

            <div class="row wow fadeIn" data-wow-delay="0.4s">

                <!-- Grid column -->
                <div class="col-md-8 col-lg-9">
                    <form action="vacantes.php" method="post" role="form" enctype="multipart/form-data">
                        <!-- Grid row -->
                        <div class="row">
                            <!-- Grid column -->
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <input type="text" id="form41" class="form-control" name="name" required>
                                    <label for="form41" class="">Nombre</label>
                                </div>
                            </div>

                            <!-- Grid column -->
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <input type="email" id="form52" class="form-control" name="email" required>
                                    <label for="form52" class="">Correo</label>
                                </div>
                            </div>
                        </div>
                        <!-- Grid row -->

                        <!-- Grid row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="md-form mb-0">
                                    <input type="text" id="form51" class="form-control" name="subject" required>
                                    <label for="form51" class="">Asunto</label>
                                </div>
                            </div>
                        </div>
                        <!-- Grid row -->

                        <!-- Grid row -->
                        <div class="row">
                            <!-- Grid column -->
                            <div class="col-md-12">

                                <div class="md-form mb-0">
                                    <textarea type="text" id="form76" class="md-textarea form-control"
                                              rows="3" name="message" required></textarea>
                                    <label for="form76">Mensaje</label>
                                </div>

                            </div>
                        </div>
                        <!-- Grid row -->

                        <br>

                        <div class="row">
                            <!-- Grid column -->
                            <div class="col-md-12">

                                <label>Seleccionar cv en formato pdf:</label>
                                <input type="file" name="pdf">

                            </div>
                        </div>

                        <div class="text-center text-md-left mb-5 mt-4">
                            <button class="btn btn-rounded btn-white font-weight-bold"><span
                                        style="color:blue;">Enviar</span></button>
                        </div>

                    </form>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-4 col-lg-3">
                    <ul class="list-unstyled contact-icons float-md-right">
                        <li>
                            <i class="fas fa-map-marker-alt fa-2x title"></i>
                            <p ALIGN="justify">Blvd. Adolfo López Mateos 2349
                                Atlamaya, 01760 Ciudad de México, CDMX</p>
                        </li>

                        <li>
                            <i class="fas fa-phone fa-2x title"></i>
                            <p align="justify">Oficinas: +52 (55) 8525 3485</p>
                        </li>

                        <li>
                            <i class="fas fa-envelope fa-2x title"></i>
                            <p ALIGN="justify">flara@integrador-technology.mx</p>
                        </li>
                        <li>
                            Servicio de soporte a sus órdenes.
                        </li>
                    </ul>
                </div>
                <!-- Grid column -->

            </div>

        </section>
        <!-- Section: Contact v.2 -->

    </div>

</main>
<!-- Main content -->

<!-- Footer -->
<footer class="page-footer text-center text-md-left unique-color-dark pt-0">

    <div class="top-yellow-footer">
        <div class="container">

            <!-- Grid row -->
            <div class="row py-4 d-flex align-items-center">

                <!-- Grid column -->
                <div class="col-md-6 col-lg-5 text-center text-md-left mb-md-0">
                    <h6 class="mb-4 mb-md-0 white-text">¡Encuentranos en nuestras redes!</h6>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-6 col-lg-7 text-center text-md-right">
                    <!-- Facebook -->
                    <a class="p-2 m-2 fa-lg fb-ic ml-0"
                       href="https://www.facebook.com/Integrador-Technology-102257384514547/" rel="noreferrer"
                       target="_blank">
                        <i class="fab fa-facebook-f white-text mr-lg-4"> </i>
                    </a>
                    <!-- Twitter -->
                    <a class="p-2 m-2 fa-lg tw-ic" href="https://twitter.com/IntegradorTech1" rel="noreferrer"
                       target="_blank">
                        <i class="fab fa-twitter white-text mr-lg-4"> </i>
                    </a>
                    <!-- Linkedin -->
                    <a class="p-2 m-2 fa-lg li-ic" href="https://mx.linkedin.com/in/integrador-technology-7b31a9124"
                       rel="noreferrer" target="_blank">
                        <i class="fab fa-linkedin-in white-text mr-lg-4"> </i>
                    </a>
                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

        </div>
    </div>

    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">
        <div class="container-fluid">
            &copy; Copyright <strong>Integrador Technology</strong>. Todos los derechos reservados.
        </div>
    </div>
    <!-- Copyright -->

</footer>
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="css/mdb.min.css" rel="stylesheet">
<!-- Footer -->
<link rel="icon" type="icon" href="/img/logo.png"/>
<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
<link href="img/favicon.icon.png" alt="Integrador technology" rel="icon">
<!--<link rel="shortcut icon" type="image/x-icon" href="img/integrador.ico">-->
<link href="img/apple-touch-icon.png" alt="Integrador technology" rel="apple-touch-icon">
<link href="https://fonts.googleapis.com/css?family=Open Sans:300,300i,400,400i,700,700i|Poppins:300,400,500,700"
      rel="stylesheet">
<link rel="shortcut icon" href="img/favicon.icon.png" alt="Integrador technology">

<!-- JQuery -->
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="js/mdb.js"></script>
<script src="js/lazysizes.min.js" async=""></script>
<script>
    // Animation init
    new WOW().init();

    // Material Select Initialization
    $(document).ready(function () {
        $('.mdb-select').material_select();
    });
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5da885afdf22d913399fad62/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>

</html>
