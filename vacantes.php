<?php

include 'composer/vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;

$nombre = $_POST['name'];
$asunto = $_POST['subject'];
$mensaje = $_POST['message'];
$email = $_POST['email'];
$pdf = $_FILES['pdf']['tmp_name'];

ob_start();
$ext = PHPMailer::mb_pathinfo($_FILES['pdf']['name'], PATHINFO_EXTENSION);
$uploadfile = tempnam(sys_get_temp_dir(), hash('sha256', $_FILES['pdf']['name'])) . '.' . $ext;

$msg = "<body><h1>INTEGRADOR - Mensaje</h1><p>Este es un <strong> mensaje recibido desde la web de Integrador.</strong>.</p>
<p>Nombre: $nombre</p>
<p>Asunto: $asunto</p>
<p>Mensaje: $mensaje</p>
<p>Mensaje enviado desde el correo: $email</p>
<p></a></p></body>";
$mail = new PHPMailer();
$mail->setFrom($email, 'Web integrador');
$mail->addAddress('flara@integrador-technology.mx', ' Fernando Lara');
$mail->addBCC('lfernando@integrador-technology.mx');

$mail->Body = $msg;

$mail->IsHTML(true);
// Attach the uploaded file
$mail->AddAttachment($pdf);
if (!$mail->addAttachment($uploadfile, 'cv-' . $nombre)) {
    $msg .= 'Failed to attach file ' . $pdf;
    $mail->Subject = 'Notificacion';
}
if (!$mail->send()) {
    $msg .= 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    $mail->Subject = 'Postulacion';
    $msg .= 'Message sent!';
    ?>
    <script type="text/javascript">
        alert("Mensaje enviado!");
        location.href = "/";
    </script>
    <?php
}